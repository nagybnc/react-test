## Result:

![Result](./result.gif)

Assignment

## Page structure:

```
Header: welcome username
Post 1
  Comments
  [Add comment...]
Post 2
  Comments
  [Add comment...]
...
Footer
```

## Requirements
- create a react web app
- use webpack2
- use bootstrap
- use ES6
- `npm run build` should create the bundle
- everything should be in a `build/` directory or in the `bundle.js`, no links should refer to the `node_modules/` folder
- add routing: add an 'about' page

## Backend service
```bash
npm install -g json-server
json-server --watch db.json
```

db.json contents:
```javascript
{
  "posts": [
    { "id": 1, "title": "Post 1" },
    { "id": 2, "title": "Post 2" },
    { "id": 3, "title": "Post 3" }
  ],
  "comments": [
    { "id": 1, "body": "some comment 1", "postId": 1 },
    { "id": 2, "body": "some comment 2", "postId": 1 },
    { "id": 3, "body": "some comment 3", "postId": 2 }
  ],
  "profile": { "name": "Joe" }
}
```

example requests:
```
http://localhost:3000/posts
http://localhost:3000/posts?_embed=comments
http://localhost:3000/posts/1/comments
```

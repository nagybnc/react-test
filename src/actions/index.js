import axios from 'axios';

export const FETCH_PROFILE = 'fetch_profile';
export const FETCH_POSTS = 'fetch_posts';
export const FETCH_COMMENTS = 'fetch_comments';
export const FETCH_POST = 'fetch_post';
export const CREATE_POST = 'create_post';
export const CREATE_COMMENT = 'create_comment';
export const DELETE_POST = 'delete_post';
const ROOT_URL = 'http://localhost:3000';

export const fetchProfile = () => {
  const request = axios.get(`${ROOT_URL}/profile`);

  return {
    type: FETCH_PROFILE,
    payload: request
  };
}

export const fetchPosts = () => {
  const request = axios.get(`${ROOT_URL}/posts`);

  return {
    type: FETCH_POSTS,
    payload: request
  };
}

export const fetchComments = id => {
  const request = axios.get(`${ROOT_URL}/comments?postId=${id}`);

  return {
    type: FETCH_COMMENTS,
    payload: request
  };
}

export const createPost = values => dispatch => {
  const request = axios.post(`${ROOT_URL}/posts`, values)
    .then(res => {
      dispatch({
        type: CREATE_POST,
        payload: res.data
      });
    });
}

export const createComment = values => dispatch => {
  const request = axios.post(`${ROOT_URL}/comments`, values)
    .then(res => {
      dispatch({
        type: CREATE_COMMENT,
        payload: res.data
      });
    });
}

export const fetchPost = id => {
  const request = axios.get(`${ROOT_URL}/posts/${id}`);

  return {
    type: FETCH_POST,
    payload: request
  }
}

export const deletePost = (id, callback) => dispatch => {
  const request = axios.delete(`${ROOT_URL}/posts/${id}`)
  .then(() => callback());

 return {
   type: DELETE_POST,
   payload: id
 }
}

import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class AboutUs extends Component {

  render() {
    return (
      <div>
        <Link to="/"> Back </Link>
        <h2>About us page</h2>
      </div>
    );
  }
}

export default AboutUs;

import React, { Component, PropTypes } from 'react';

export default class CommentsFormComponent extends Component {
  state = {
    body: ''
  };

  onSubmit(e) {
    e.preventDefault();
    const { postId } = this.props;
    const { body } = this.state;

    if(body !== '') {
        this.props.onSubmit({ postId, body });
        this.setState({
          body: ''
        });
    }

  }

  render() {
    const { body } = this.state;

    return (
      <form onSubmit={ this.onSubmit.bind(this) }>
          <input
            type="text"
            placeholder="Type comment..."
            value={ body }
            onChange={ e => this.setState({ body: e.target.value })}
          />
          <button type="submit" className={`btn ${body === '' ? 'btn-default' : 'btn-success'}`} disabled={ body === '' ? true : false } >Submit</button>
      </form>
    );
  }
}

CommentsFormComponent.propTypes = {
  postId: PropTypes.number.isRequired
}

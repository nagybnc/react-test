import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { fetchComments } from '../actions';

export default class CommentsListComponent extends Component {
  componentWillMount() {
    this.props.fetchComments(this.props.postId);
  }

  renderPosts() {
    const { comments } = this.props;

    if (comments.length === 0) {
      return <div>No Comments</div>
    }

    return comments.map((comment, idx) => {
      return (
        <li className="list-group-item" key={idx}>
            {comment.body}
        </li>
      )
    });
  }

  render() {
    return (
      <div>
        <h3> Comments </h3>
        <ul className="list-group">
         {this.renderPosts()}
        </ul>
      </div>
    );
  }
}

CommentsListComponent.propTypes = {
  postId: PropTypes.number.isRequired,
  comments: PropTypes.array.isRequired
}

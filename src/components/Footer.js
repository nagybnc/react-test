import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class footer extends Component {

  render() {
    return (
      <div>
        <footer className="footer">
          <div className="container">
            <Link to="/about"> About Us </Link>
          </div>
        </footer>
      </div>
    );
  }
}

export default footer;

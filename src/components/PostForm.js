import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createPost } from '../actions'

export default class PostFormComponent extends Component {
  componentWillReceiveProps(nextProps) {
    if(nextProps.submitSucceeded) {
      this.props.history.push('/');
    }
  }
  renderField(field) {
    const { meta: { touched, error } } = field;
    const classNametoGroup = `form-group ${ touched && error ? 'has-danger' : touched && !error ? 'has-success' : ''}`;
    const classNametoControl = `form-control ${ touched && error ? 'form-control-danger' : touched && !error ? 'form-control-success' : ''}`;

    return (
      <div className={classNametoGroup}>
        <label> {field.label} </label>
        <input
          className={classNametoControl}
          type="text"
          {...field.input}
        />
        <div className="text-help">
          {touched ? error : ''}
        </div>
      </div>
    )
  }

  onSubmit(values) {
    this.props.createPost(values);
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
      <Field
        label="Title For Post"
        name="title"
        component={this.renderField}
      />
      <Field
        label="Post Content"
        name="content"
        component={this.renderField}
      />
      <button type="submit" className="btn btn-success">Submit</button>

      <Link to="/" className="btn btn-danger">Cancel</Link>

        </form>
    );
  }
}

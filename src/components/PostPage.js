import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { fetchPost, deletePost } from '../actions';

import CommentsList from '../containers/CommentsList';
import CommentsForm from '../containers/CommentsForm';

export default class PostPage extends Component {
  componentWillMount(){
    const { match: { params: { id } } } = this.props;
    this.props.fetchPost(id);
  }

  onDeleteClick() {
    const { match: { params: { id } } } = this.props;
    this.props.deletePost(id, () => {
      this.props.history.push('/');
    });
  }

  render() {
    const { post, match: { params: { id } } } = this.props;

    if (!post) {
      return <div>Loading...</div>;
    }

    return (
      <div>
        <div className="post-show-header">
          <Link to="/"> Back to index </Link>
          <button
            className= "btn btn-danger"
            onClick={this.onDeleteClick.bind(this)}
          >
            Delete Post
          </button>
        </div>
        <h3>{post.title}</h3>
        <p>{post.content}</p>
        <hr />
        <CommentsList postId={ parseInt(id, 10) } />
        <hr />
        <CommentsForm postId={ parseInt(id, 10) } />
      </div>
    );
  }
}

import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class PostsListComponent extends Component {
  componentDidMount() {
    this.props.fetchPosts();
  }

  renderPosts() {
    const { posts } = this.props;
    
    return posts.map(post => {
      return (
        <li className="list-group-item" key={post.id}>
          <Link to={`/posts/${post.id}`}>
            {post.title}
          </Link>
        </li>
      )
    });
  }

  render() {
    return (
      <div>
        <div className="text-xs-right">
          <Link className="btn btn-success" to="/posts/new">
            New Post
          </Link>
        </div>
        <h3> POSTS </h3>
        <ul className="list-group">
         {this.renderPosts()}
        </ul>
      </div>
    );
  }
}

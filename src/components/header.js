import React, { Component } from 'react';

export default class Header extends Component {
  componentWillMount() {
    this.props.fetchProfile();
  }

  render() {
    return (
      <header>
        <div className="container">
        Hello <strong>{this.props.profile.name}</strong> !
        </div>
      </header>
    );
  }
}

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createComment } from '../actions';
import CommentsFormComponent from '../components/CommentsForm';

class CommentsForm extends Component {
  render() {
    return (
      <CommentsFormComponent { ...this.props } />
    );
  }
}

export default connect(null, { onSubmit: createComment })(CommentsForm)

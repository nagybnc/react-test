import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchComments } from '../actions';
import CommentsListComponent from '../components/CommentsList';

class CommentsList extends Component {
  render() {
    return (
      <CommentsListComponent { ...this.props } />
    );
  }
}

function mapStateToProps({ comments }) {
  return {
    comments
  };
}

export default connect(mapStateToProps, { fetchComments })(CommentsList);

import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createPost } from '../actions';

import PostFormComponent from '../components/PostForm';

class PostForm extends Component {
  render() {
    return (
      <PostFormComponent { ...this.props } />
    )
  }
}

function validate(values) {

  const errors = {};

  if (!values.title || values.title.length < 3) {
    errors.title = "Enter a title that is least 3 characters";
  }

  if (!values.content) {
    errors.content = "Enter some content";
  }

  return errors;
}

export default reduxForm({
  validate,
  form: 'PostForm'
})(
  connect(null,{ createPost })(PostForm)
);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPost, deletePost } from '../actions';

import PostPageComponent from '../components/PostPage';

class PostPage extends Component {
  render() {
    return (
      <PostPageComponent { ...this.props } />
    );
  }
}

function mapStateToProps({ posts }) {
  return { post: posts.active }
}

export default connect(mapStateToProps, { fetchPost, deletePost })(PostPage);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPosts } from '../actions';
import PostsListComponent from '../components/PostsList';

class PostsList extends Component {
  render() {
    return (
      <PostsListComponent { ... this.props } />
    );
  }
}

function mapStateToProps(state) {
  return { posts: state.posts.all };
}

export default connect(mapStateToProps, { fetchPosts })(PostsList);

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchProfile } from '../actions';

import HeaderComponent from '../components/Header';

class Header extends Component {
  render() {
    return (
      <HeaderComponent { ...this.props } />
    );
  }
}

function mapStateToProps({ profile }) {
  return {
    profile
  };
}

export default connect(mapStateToProps, { fetchProfile })(Header);

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import thunk from 'redux-thunk';
import promise from 'redux-promise';

import reducers from './reducers';
import Header from './containers/Header';
import Footer from './components/Footer';
import AboutUs from './components/AboutUs';
import PostsList from './containers/PostsList';
import PostForm from './containers/PostForm';
import PostPage from './containers/PostPage';

const middleWares = [promise, thunk];
const createStoreWithMiddleware = applyMiddleware(...middleWares)(createStore);

import '../style/bootstrap.min.css';
import '../style/style.css';

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <div>
        <Route path="*" component={Header} />
          <hr />
          <div className="container">
            <Switch>
              <Route exact path="/" component={PostsList} />
              <Route path="/posts/new" component={PostForm} />
              <Route path="/posts/:id" component={PostPage} />
              <Route path="/about" component={AboutUs} />
            </Switch>
          </div>
        <Route path="*" component={Footer} />
      </div>
    </BrowserRouter>
  </Provider>
  , document.querySelector('#wrapper'));

import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import PostsReducer from './reducer_posts';
import ProfileReducer from './reducer_profile';
import CommentsReducer from './reducer_comments';

const rootReducer = combineReducers({
  profile: ProfileReducer,
  posts: PostsReducer,
  comments: CommentsReducer,
  form: formReducer
});

export default rootReducer;

import {
  FETCH_COMMENTS,
  CREATE_COMMENT
} from '../actions';

const initialState = [];

export default function(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case FETCH_COMMENTS:
      return payload.data;
      break;
    case CREATE_COMMENT:
      return [...state, payload]
    default:
      return state;
  }
}

import _ from 'lodash';
import {
  CREATE_POST,
  FETCH_POSTS,
  FETCH_POST,
  DELETE_POST
} from '../actions';

const initialState = {
  all: [],
  active: null
};

export default function(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case CREATE_POST:
      return {
        all: [...state.all, payload],
        active: payload
      };
      break;
    case DELETE_POST:
      return _.omit(state, action.payload);
      break;
    case FETCH_POST:
      return Object.assign({}, state, { active: payload.data });
      break;
    case FETCH_POSTS:
      return Object.assign({}, state, { all: payload.data });
      break;
    default:
      return state;
  }
}
